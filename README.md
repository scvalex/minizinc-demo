minizinc-demo
=============

In which I experiment with minizinc, mostly by following the [official
tutorial][tut].

[tut]: https://www.minizinc.org/doc-2.7.3/en/part_2_tutorial.html
