aust:
	minizinc aust.mzn

cakes:
	minizinc cakes2.mzn pantry.dzn

loan:
	minizinc --solver cbc loan.mzn loan3.dzn

sudoku:
	minizinc --statistics sudoku.mzn

partial:
	minizinc --all-solutions partial.mzn

partial2:
	minizinc --all-solutions partial2.mzn

rect-packing:
	minizinc rect-packing.mzn rect-packing.json
